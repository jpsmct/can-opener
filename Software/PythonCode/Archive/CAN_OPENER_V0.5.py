#!/usr/bin/python3
import math
import sys
import select
import threading
import time
import serial
import os
import time
import mysql.connector
import csv
import can
#import serial.tools.list_ports
#from serial.tools.list_ports import comports



slp_time = 0.5 #seconds
T_min = 10*slp_time
T1 = T_min*1
T2 = T_min*2
T3 = T_min*2.5
T4 = T_min*3
T5 = T_min*3.5
T6 = T_min*1
T7 = T_min*2



class shared():
    pass
shar_var = shared()
shar_var.input_val = '-1'
shar_var.prev_scan = '-1'

def user_input():
    while True:
        value = sys.stdin.readline().rstrip()
        shar_var.input_val = value
        # print("Input received: ")
        # print(value)

        
def CANnect():
    os.system('sudo ifconfig can1 down')
    os.system('sudo ip link set can1 type can bitrate 100000')
    os.system('sudo ifconfig can1 up')

def CAN_Tx(data, ID):
    try:
        msg = can.Message(arbitration_id=ID, data=list(int(data).to_bytes(3, 'big')), is_extended_id=False)
        can1.send(msg)
    except:
        CANnect()
        msg = can.Message(arbitration_id=ID, data=list(int(data).to_bytes(3, 'big')), is_extended_id=False)
        can1.send(msg)
#can1 = can.interface.Bus(channel = 'can1', bustype = 'socketcan')# socketcan_native    
    
#coms= os.path.join('/dev', comports()[0].name)
#arduino = serial.Serial(port=coms, baudrate=115200, timeout=.1)

# Note, this assumes 'mydatabase' database exists
mydb = mysql.connector.connect(
  host="localhost",
  user="pi",
  password="raspberry",
  database="CANOPENER"
)



# Instantiate and connect to cursor object:
mycursor = mydb.cursor()

sqlinsert = """INSERT INTO DUMMYDATA (TIME, DATA1, DATA2, DATA3, DATA4, DATA5, DATA6, DATA7, DATA8, DATA9, DATA10,
                                      DATA11, DATA12, DATA13, DATA14, DATA15, DATA16, DATA17, DATA18, DATA19,
                                      DATA20) 
                              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                                      %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

mycursor.execute("TRUNCATE TABLE DUMMYDATA")



# Establish CAN
CANnect()
can1 = can.interface.Bus(channel = 'can1', bustype = 'socketcan')# socketcan_native






# now threading1 runs regardless of user input
threading1 = threading.Thread(target=user_input)
#threading1.daemon = True
threading1.start()

startTime = time.time()

while True:
    if shar_var.input_val == '0':
        print("Killing script")
        mycursor.execute("TRUNCATE TABLE DUMMYDATA")
        shar_var.input_val = '-1'
        shar_var.prev_scan = '0'
        os.system('sudo ifconfig can1 down')
    elif shar_var.input_val == '1':
        print("Acquiring Data...")
        if shar_var.prev_scan == '0':
            CANnect()
            startTime = time.time()
            shar_var.prev_scan = '1'
        t = time.time()-startTime
        # AMS Voltages
        AMS_min = 288
        AMS_max = 336
        d1 = (AMS_max-AMS_min)/2*(math.sin(math.pi*(2*1/T1*(t+1.5)))+1)+AMS_min #Pack Voltage
        CAN_Tx(d1, 0x01)
        d2 = (AMS_max-AMS_min)/2*(math.sin(math.pi*(2*1/T1*(t-1.5)))+1)+AMS_min #Vehicle Voltage
        CAN_Tx(d2, 0x02)
        d3 = (AMS_max-AMS_min)/2*(math.sin(math.pi*(2*1/T1*(t-0)))+1)+AMS_min #Pack Current
        CAN_Tx(d3, 0x03)
        #Motor Temps
        MT_min = 0
        MT_max = 100
        d4 = (MT_max-MT_min)/2*(math.sin(math.pi*(2*1/T2*(t+1.5)))+1)+MT_min #Control Board Temp
        CAN_Tx(d4, 0x04)
        d5 = (MT_max-MT_min)/2*(math.sin(math.pi*(2*1/T2*(t-1.5)))+1)+MT_min #Motor Temp
        CAN_Tx(d5, 0x05)
        #Motor Digitals
        d6 = (MT_max-MT_min)/2*(math.sin(math.pi*(2*1/T3*(t-1.5)))+1)+MT_min #D1, Forward
        CAN_Tx(d6, 0x06)
        d7 = (MT_max-MT_min)/2*(math.sin(math.pi*(2*1/T3*(t+1.5)))+1)+MT_min #D2, Reverse
        d8 = (MT_max-MT_min)/2*(math.sin(math.pi*(2*1/T3*(t+0)))+1)+MT_min #D3, Brake
        d9 = 1 if (math.sin(math.pi*(2*1/T4*(t+1)))>0) else 0 #D4, Regen Disable
        d10 = 1 if (math.sin(math.pi*(2*1/T4*(t-1)))>0) else 0 #D5, Ignition Switch
        d11 = 1 if (math.sin(math.pi*(2*1/T4*(t+0)))>0) else 0 #D6, Start Switch
        #Motor Position
        MP_min = 0
        MP_max = 5500
        d12 = (MP_max-MP_min)/2*(math.sin(math.pi*(2*1/T5*(t+0)))+1)+MP_min #Angle Velocity
        #Motor Current
        MC_min = 0
        MC_max = 450
        d13 = (MC_max-MC_min)/2*(math.sin(math.pi*(2*1/T5*(t+1.5)))+1)+MC_min #DC Current
        #Motor Voltage
        MV_min = 0
        MV_max = 400
        d14 = (MV_max-MV_min)/2*(math.sin(math.pi*(2*1/T6*(t-1.5)))+1)+MV_min #DC Bus Voltage
        d15 = (MV_max-MV_min)/2*(math.sin(math.pi*(2*1/T6*(t+1.5)))+1)+MV_min #Output Voltage
        #Pedal Board
        PB_min = 0
        PB_max = 100
        d16 = (PB_max-PB_min)/2*(math.sin(math.pi*(2*1/T7*(t+0)))+1)+PB_min #Throttle
        d17 = (PB_max-PB_min)/2*(math.sin(math.pi*(2*1/T7*(t+1.5)))+1)+PB_min #Brake
        d18 = (PB_max-PB_min)/2*(math.sin(math.pi*(2*1/T7*(t-1.5)))+1)+PB_min #Steering
        #Extras
        d19 = -1
        d20 = -1

        entry = [t,d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20]
        mycursor.execute(sqlinsert, entry)
        mydb.commit()
        
    else:
        pass
    time.sleep(slp_time)
