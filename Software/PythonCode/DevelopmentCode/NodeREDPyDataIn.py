#!/usr/bin/python3
import sys
import json
import serial
import os
import time
import mysql.connector
import csv
import serial.tools.list_ports
from serial.tools.list_ports import comports

def getpayload(payload)
    return payload


print("Hello!")
#run = sys.argv[1]
#run1 = '{{payload}}'
#run1 = sys.stdin.readlines()
run1 = payload
run = 0
print(run1)

coms= os.path.join('/dev', comports()[0].name)
arduino = serial.Serial(port=coms, baudrate=115200, timeout=.1)

# Note, this assumes 'mydatabase' database exists
mydb = mysql.connector.connect(
  host="localhost",
  user="pi",
  password="raspberry",
  database="mydatabase"
)

# Instantiate and connect to cursor object:
mycursor = mydb.cursor()

sqlinsert = "INSERT INTO MYTABLE (LIGHT, POT, TIME) VALUES (%s, %s, %s)"

mycursor.execute("TRUNCATE TABLE MYTABLE")

startTime = time.time()
while run:
    if run ==2:
        mycursor.execute("TRUNCATE TABLE MYTABLE")
        break
      
    tempstr = arduino.readline().decode('utf-8').rstrip()[1:]
    entry = [j for j in tempstr.split(',')]
    entry.append(time.time()-startTime)
    print(sqlinsert, entry)
    #mycursor.execute(sqlinsert, entry)
    mydb.commit()
    time.sleep(0.2)
    #run = sys.stdin.readlines()
    #run = '{{payload}}'
    print(run)

mydb.close()