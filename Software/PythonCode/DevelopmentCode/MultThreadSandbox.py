#!/usr/bin/python3
import sys
import select
import threading
import time

class shared():
    pass

shar_var = shared()
shar_var.input_val = '-1'

print("SCRIPT STARTING")

def user_input():
    while True:
        value = sys.stdin.readline().rstrip()
        shar_var.input_val = value
        print("Input received: ")
        print(value)

def DAQ():
    if shar_var.input_val == '0':
        print("Killing script")
    elif shar_var.input_val == '1':
        print("Acquiring Data...")
    elif shar_var.input_val == '2':
        print("intput was 2")
        pass
    else:
        print("Unknown input?")
    time.sleep(1)

# now threading1 runs regardless of user input
threading1 = threading.Thread(target=user_input)
#threading1.daemon = True
#threading2 = threading.Thread(target=DAQ) 
threading1.start()
#threading2.start()

while True:
    if shar_var.input_val == '0':
        print("Killing script")
        break
    elif shar_var.input_val == '1':
        print("Acquiring Data...")
    elif shar_var.input_val == '2':
        print("intput was 2")
        pass
    else:
        print("no input")
    time.sleep(1)
