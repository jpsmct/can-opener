//The following is used in conjunction with the ADC2CAN_VX.X.ipynb. 
*/ This program is intended for the Arduino Mega (2560 variant), but may
work on similar arduino devices. ADC channels 1-6 are configured with internal 
pullup resistors and are used as sources for generating serial traffic which is
then converted to CAN traffic by the ADC2CAN script. Analog sensors can be 
connected to pins A1-A6 and ground. 

The last data point in each data frame represents a data point from a sinusoid
- the visual appearance of the sinusoid on the receiving end may be used to 
identify how much data is lost or if the data is being received correctly.
/*


int ctr = 0;

void setup() {
  Serial.begin(115200);
  pinMode(A1, INPUT_PULLUP);
  pinMode(A2, INPUT_PULLUP);
  pinMode(A3, INPUT_PULLUP);
  pinMode(A4, INPUT_PULLUP);
  pinMode(A5, INPUT_PULLUP);
  pinMode(A6, INPUT_PULLUP);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue1 = analogRead(A1);
  int sensorValue2 = analogRead(A2);
  int sensorValue3 = analogRead(A3);
  int sensorValue4 = analogRead(A4);
  int sensorValue5 = analogRead(A5);
  int sensorValue6 = analogRead(A6);
  ctr = ctr + 1;  
  if (ctr > 360) ctr = 0;
  int sinusoid = int(1024*(sin(ctr*15*3.1415/180)+1)/2);

  // print out the value you read:
  Serial.flush();
  Serial.print('#');
  Serial.print(1024-sensorValue1);
  Serial.print(',');
  Serial.print(1024-sensorValue2);
  Serial.print(',');
  Serial.print(1024-sensorValue3);
  Serial.print(',');
  Serial.print(1024-sensorValue4);
  Serial.print(',');
  Serial.print(1024-sensorValue5);
  Serial.print(',');
  Serial.print(1024-sensorValue6);
  Serial.print(',');
  Serial.println(sinusoid);
  delay(200);        // delay in between reads for stability
}
