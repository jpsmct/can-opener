#!/usr/bin/python3
import sys
import select
import threading
import time
import serial
import os
import time
import mysql.connector
import csv
import serial.tools.list_ports
from serial.tools.list_ports import comports

class shared():
    pass
shar_var = shared()
shar_var.input_val = '-1'

def user_input():
    while True:
        value = sys.stdin.readline().rstrip()
        shar_var.input_val = value
        # print("Input received: ")
        # print(value)
    
coms= os.path.join('/dev', comports()[0].name)
arduino = serial.Serial(port=coms, baudrate=115200, timeout=.1)

# Note, this assumes 'mydatabase' database exists
mydb = mysql.connector.connect(
  host="localhost",
  user="pi",
  password="raspberry",
  database="mydatabase"
)

# Instantiate and connect to cursor object:
mycursor = mydb.cursor()

sqlinsert = "INSERT INTO MYTABLE (LIGHT, POT, TIME) VALUES (%s, %s, %s)"

mycursor.execute("TRUNCATE TABLE MYTABLE")

# now threading1 runs regardless of user input
threading1 = threading.Thread(target=user_input)
#threading1.daemon = True
threading1.start()

startTime = time.time()

while True:
    if shar_var.input_val == '0':
        print("Killing script")
        mycursor.execute("TRUNCATE TABLE MYTABLE")
        shar_var.input_val = '2'
    elif shar_var.input_val == '1':
        print("Acquiring Data...")
        while True:
            tempstr = arduino.readline().decode('utf-8').rstrip()[1:]
            entry = [j for j in tempstr.split(',')]
            print(entry)
            # Check type, confirm it is float or int:
            invalid = 0
            if len(entry) == 3:
                for i in range(len(entry)):
                    if not (type(entry[i]) == float or type(entry[i]) == int):
                        invalid = invalid+1
                if not invalid:
                    break
        # Append data to db:
        entry.append(time.time()-startTime)
        mycursor.execute(sqlinsert, entry)
        mydb.commit()
    else:
        pass
    time.sleep(0.5)
