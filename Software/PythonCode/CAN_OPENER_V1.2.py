#!/usr/bin/python3
import math
import sys
import select
import threading
import time
import serial
import os
import time
import mysql.connector
#import csv
import can


class shared(): #Used to share input values between threads
    pass
shar_var = shared()
shar_var.input_val = '-1'
shar_var.prev_scan = '-1'

def user_input(): #Catches user inputs
    while True:
        value = sys.stdin.readline().rstrip()
        shar_var.input_val = value
        # print("Input received: ")
        # print(value)

def CANnect(): #(Re)establishes CAN network adapter
    os.system('sudo ifconfig can0 down')
    os.system('sudo ip link set can0 type can bitrate 500000')
    os.system('sudo ifconfig can0 txqueuelen 1000')
    os.system('sudo ifconfig can0 up')


# Note, this assumes 'CANOPENER' database exists
mydb = mysql.connector.connect(
  host="localhost",
  user="pi",
  password="raspberry",
  database="CANOPENER"
)

# Instantiate and connect to cursor object:
mycursor = mydb.cursor()

# Check if the table exists, and if not create it:
mycursor.execute("SHOW TABLES LIKE 'ADC_DATA'")
result = mycursor.fetchone()
if result == None:
    dummydata = """CREATE TABLE ADC_DATA (
             id INT AUTO_INCREMENT PRIMARY KEY,
             TIME FLOAT(10),
             DATA1 FLOAT(10),
             DATA2 FLOAT(10),
             DATA3 FLOAT(10),
             DATA4 FLOAT(10),
             DATA5 FLOAT(10),
             DATA6 FLOAT(10),
             DATA7 FLOAT(10))"""
    mycursor.execute(dummydata)

# Templated SQL insert string, to be used with .execute() and .commit()
sqlinsert = """INSERT INTO ADC_DATA (TIME, DATA1, DATA2, DATA3, DATA4, DATA5, DATA6, DATA7) 
                              VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""

# Establish CAN
CANnect()
can0 = can.interface.Bus(channel = 'can0', bustype = 'socketcan')# socketcan_native

# now threading1 runs regardless of user input
threading1 = threading.Thread(target=user_input)
#threading1.daemon = True
threading1.start()
#print("not oops")
while True:
    if shar_var.input_val == '0':
        print("Killing script")
        mycursor.execute("TRUNCATE TABLE ADC_DATA")
        shar_var.input_val = '-1'
        shar_var.prev_scan = '0'
        #os.system('sudo ifconfig can0 down')
    elif shar_var.input_val == '1':
        print("Acquiring Data...")
        if shar_var.prev_scan <= '0': #If script was previously stopped (0) or first time running (-1), restart CAN and set new startTime 
            #try:
            #    CANnect()
            #except:
            #    pass
            startTime = time.time()
            shar_var.prev_scan = '1'
        #Acquire CAN frames, decode, and insert into DB:
        d1 = None
        d2 = None
        d3 = None
        d4 = None
        d5 = None
        d6 = None
        d7 = None
        msg = None
        #print(sampled)
        i = 0
        while i<7:
#            CANnect()
            msg = can0.recv(1)
            #print(msg)
            if msg != None:
                ID = msg.arbitration_id
                data = int.from_bytes(msg.data, byteorder='big')
                #print(hex(data))
                #if shar_var.input_val != '1':
                #    print("oops")
                #    break
                if ID==0x20:
                    d1 = (msg.data[0] << 8) | msg.data[1] 
                    d2 = (msg.data[2] << 8) | msg.data[3] 
                    d3 = (msg.data[4] << 8) | msg.data[5] 
                    d4 = (msg.data[6] << 8) | msg.data[7]
                    d6 = 4432
                    #print(msg.data[0], msg.data[1], msg.data[2], msg.data[3])
    #            elif ID==2:
    #                d2 = data
    #            elif ID==3:
    #                d3 = data
    #            elif ID==4:
    #                d4 = data
    #            elif ID==5:
    #                d5 = data
    #            elif ID==6:
    #                d6 = data
    #                #print(d6)
    #            elif ID==7:
    #                d7 = data
                else:
                    i -=1 # Cancel post-increment if no IDs matched
                i += 1
                #print(d1)
            else:
                pass
        #entry = [d1, d2, d3, d4, d5, d6, d7]
        #print(int.from_bytes(msg.data, byteorder='big'))
        #print(entry)
        #print(d7)
        
        #Insert entry into db: 
        t = time.time()-startTime
        entry = [t,d1,d2,d3,d4,d5,d6,d7]
        mycursor.execute(sqlinsert, entry)
        mydb.commit()    
    else:
        trash = can0.recv(1) # Keeps buffer empty
        pass
