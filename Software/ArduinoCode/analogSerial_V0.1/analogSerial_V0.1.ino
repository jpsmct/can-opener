void setup() {
  Serial.begin(115200);
  pinMode(A1, INPUT_PULLUP);
  pinMode(A2, INPUT_PULLUP);
  pinMode(A3, INPUT_PULLUP);
  pinMode(A4, INPUT_PULLUP);
  pinMode(A5, INPUT_PULLUP);
  pinMode(A6, INPUT_PULLUP);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue1 = analogRead(A1);
  int sensorValue2 = analogRead(A2);
  int sensorValue3 = analogRead(A3);
  int sensorValue4 = analogRead(A4);
  int sensorValue5 = analogRead(A5);
  int sensorValue6 = analogRead(A6);

  // print out the value you read:
  Serial.flush();
  Serial.print('#');
  Serial.print(1024-sensorValue1);
  Serial.print(',');
  Serial.print(1024-sensorValue2);
  Serial.print(',');
  Serial.print(1024-sensorValue3);
  Serial.print(',');
  Serial.print(1024-sensorValue4);
  Serial.print(',');
  Serial.print(1024-sensorValue5);
  Serial.print(',');
  Serial.println(1024-sensorValue6);
  delay(200);        // delay in between reads for stability
}
